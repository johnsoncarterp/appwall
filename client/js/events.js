"use strict";

Template.form.events({
    'submit .add-new-post': function (event) {
        event.preventDefault();


        var postName = event.currentTarget.children[0].children[0].value;
        var postMessage = event.currentTarget.children[1].children[0].value;
        var postImage = event.currentTarget.children[2].children[0].children[1].files[0];
        if (postImage === undefined) {
        alert("Empty post please type something")
        }
        
        Collections.Images.insert(postImage, function (error, fileObject) {
            if (!postName.trim() || !postMessage.trim()) {
                alert("Empty post please type something")
            } else {
                Collections.Post.insert({
                    author: postName,
                    message: postMessage,
                    image_id: fileObject._id,
                    createdAt: new Date()
                });
                $('.grid').masonry('reloadItems');
            }

        });

    }
});